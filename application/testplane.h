#ifndef TESTPLANE_H
#define TESTPLANE_H


#include <parametrics/gmpplane>


class TestPlane : public GMlib::PPlane<float> {
public:
    using PPlane::PPlane;

const GMlib::Point<float,3>& getCornerPoint();


};

    //////////////////////////////////////////////////////////
    /// \brief TestPlane::PPlane


    const GMlib::Point<float, 3> &TestPlane::getCornerPoint()
    {
      return this->_pt;
    }
   #endif
