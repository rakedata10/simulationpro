#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "testplane.h"
#include "firstball.h"
#include "collobj.h"

#include <core/gmarray>
#include <parametrics/gmpbeziersurf>

class Controller:public GMlib::PSphere<float>{
    GM_SCENEOBJECT(PSphere)

 private:

        GMlib::Array <Collobj> coll;
        GMlib::Array<FirstBall*> _b;
        GMlib::Array<TestPlane*> _w;
        GMlib::PBezierSurf<float>* surf;
        GMlib::Array<FirstBall*> _x;


public:

    Controller(GMlib::PBezierSurf<float>* surf);

    void insertBall(FirstBall *b);
    void insertTestPlane(TestPlane *w);


protected:

    void findBBcoll(FirstBall *b1, FirstBall *b2, GMlib::Array<Collobj> &co, float x);
    void findBWcoll(FirstBall *b, TestPlane *w, GMlib::Array<Collobj> &co, float x);

    void colBW(FirstBall *b, TestPlane *w, double dt_c);
    void colBB(FirstBall *b1, FirstBall *b2, double dt_c);
    void localSimulate(double dt);
    void update(float x);

};



// content of controller.c files


void Controller::insertBall(FirstBall *b)
{
    this->insert(b);
    _b += b;
}

void Controller::insertTestPlane(TestPlane *w)
{
    this->insert(w);
    _w += w;
}


void Controller::localSimulate(double dt){

    for (int i=0; i<_b.size();i++)
        _b[i] ->update_step(dt);


    for (int i=0; i < _b.size(); i++)
        for (int j=i+1; j< _b.size(); j++)
            findBBcoll(_b[i], _b[j], coll, 0);

    for (int i=0; i < _b.size(); i++)
        for (int j=0; j< _w.size(); j++)
            findBWcoll(_b[i], _w[j], coll, 0);

    while(coll.getSize()>0)
    {
//        std::cout << "collision: " << coll.getSize() << std::endl;
        coll.sort();
        coll.makeUnique();

        Collobj co = coll[0];
        coll.removeFront();

        if(co.getIsBw()){

            colBW(co.getBalll(0), co.getWall(), (1-co.getX())*dt);
            co.getBalll(0)->update(co.getX());

            for (int i = 0; i < _b.size(); i++ )
                if(_b[i] != co.getBalll(0))
                    findBBcoll(_b[i], co.getBalll(0), coll, co.getX());
            for (int i = 0; i <_w.size(); i++ )
                if(_w[i] != co.getWall())
                    findBWcoll(co.getBalll(0), _w[i], coll, co.getX());
        }
        else
        {
            colBB(co.getBalll(0), co.getBalll(1), (1-co.getX())*dt);
            co.getBalll(0)->update(co.getX());
            co.getBalll(1)->update(co.getX());

            for (int i = 0; i < _b.size(); i++ )
                if(_b[i] != co.getBalll(0) && _b[i] != co.getBalll(1))
                {
                    findBBcoll(_b[i], co.getBalll(0), coll, co.getX());
                    findBBcoll(_b[i], co.getBalll(1), coll, co.getX());
                }
            for (int i = 0; i <_w.size(); i++ )
                if(_w[i] != co.getWall())
                {
                    findBWcoll(co.getBalll(0), _w[i], coll, co.getX());
                    findBWcoll(co.getBalll(1), _w[i], coll, co.getX());
                }
        }
    }
}

Controller::Controller(GMlib::PBezierSurf<float>* surf){

    this->toggleDefaultVisualizer();
    this->replot(5,5,0,0);
    this->setVisible(false);
    this->surf=surf;
    this->insert(surf);
}



// collision of balls//


void Controller::findBBcoll(FirstBall *b1,FirstBall *b2, GMlib::Array<Collobj> &co, float px){

    GMlib::Vector<float,3> h = b1->get_ds() - b2->get_ds();
    GMlib::Point<float,3>   s = b1->getCenterPos()-b2->getCenterPos();

    double a = h*h;
    double b =s*h;
    double r =b1->getRadius() + b2->getRadius();
    double c =s*s - r*r;

    double k = b*b-a*c;


    if (k>0)
    {
        if (std::abs(a)<0.0000001f){
            return ;
        }
        else
        {

            double x= (-b-sqrt(k))/a;
            if (px < x && x <= 1){
                //            std::cout << "x" << x << std::endl;
                co += Collobj(b1,b2,x);
            }

        }
    }
}


void Controller::findBWcoll(FirstBall *b1, TestPlane *w, GMlib::Array<Collobj> &co, float px){

    GMlib::Point<float,3>  p = b1->getPos();
    GMlib::Vector<float,3> d = w->getCornerPoint() - p;

    GMlib::Vector<float,3> ds = b1->get_ds();
    GMlib::Vector<float,3>  n = w->getNormal();
    double a=ds*n;
    double b=d*n;
    double r=b1->getRadius();

    double x=(r+b)/a;


    //considering the differnet case
    if(px < x && x <= 1) {
        co.insertAlways(Collobj(b1,w,x));
    }
}

//ball and wall collision

void Controller::colBW(FirstBall *b, TestPlane *w, double dt_c){
    GMlib::Vector<float,3> new_v = b->get_vel();
    new_v -= 2*(b->get_vel()*w->getNormal())*w->getNormal();;
    b->set_v(new_v);
    b->update_step(dt_c);
}

// ball ball collision
void Controller::colBB(FirstBall *b1, FirstBall *b2, double dt_c){

    GMlib::Vector<float, 3> new_v1 = b1->get_vel();
    GMlib::Vector<float, 3> new_v2 = b2->get_vel();

    GMlib::UnitVector<float, 3> d = b2->getPos() - b1->getPos();

    float dd =d*d;

    GMlib::Vector<float, 3> v1 = ((b1->get_vel() * d)/dd)*d;
    GMlib::Vector<float, 3> v1n = b1->get_vel() - v1;

    GMlib::Vector<float, 3> v2 = ((b2->get_vel() * d)/dd)*d;
    GMlib::Vector<float, 3> v2n = (b2->get_vel()) - v2;

    float m1 = b1->get_m();
    float m2 = b2->get_m();

    GMlib::Vector<float, 3> _v1 = ((m1 - m2)/(m1 + m2))*v1 + ((2*m2)/(m1 + m2))*v2;
    GMlib::Vector<float, 3> _v2 = ((m2 - m1)/(m1 + m2))*v2 + ((2*m1)/(m1 + m2))*v1;

    new_v1 = _v1 + v1n;
    new_v2 = _v2 + v2n;


    //set velocity for ball1
    b1->set_v(new_v1);

    //update ds
    b1->update_step(dt_c);

    //set velocity for ball2
    b2->set_v(new_v2);
    //update ds
    b2->update_step(dt_c);

}




#endif // CONTROLLER_H


