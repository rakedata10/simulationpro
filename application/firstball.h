#ifndef TESTTORUS_H
#define TESTTORUS_H
#include"testplane.h"
#include <parametrics/gmpsphere>

#include <QDebug>


class FirstBall : public GMlib::PSphere<float> {

public:

    using PSphere::PSphere;

    //    FirstBall(GMlib::Vector<float,3> v, float m, float r, TestPlane* s);
    FirstBall(GMlib::Vector<float,3> v, float m, float r, PSurf<float,3>* s);
    void update_step(double dt);
    void update(float x);
    void localSimulate(double dt);
    void set_v(const GMlib::Vector<float,3>& _setv);

    void bizzare(const GMlib::Vector<float,3>& p);
    const GMlib::Vector<float,3>& get_ds()const;
    const GMlib::Vector<float,3>& get_vel()const;

    float get_m();

private:
    GMlib::UnitVector<float,3> n;
    GMlib::Vector<float,3> ds;
    GMlib::UnitVector<float,3> rolling_b;
    GMlib::Point<float,3> p;
    GMlib::Point<float,3> q;
    GMlib::Vector<float,3> v;

    float _u;
    float _v;
    float m;

    double _dt;
    double x;

    GMlib::PSurf<float,3>* surf;


};


//////////////////////////////////////////////////////////
/// \brief FirstBall::FirstBall
/// \param v
/// \param m
/// \param r
/// \param s
///////////////////////////////////////////////////



FirstBall::FirstBall(GMlib::Vector<float,3> v, float m, float r, PSurf<float,3>* s):GMlib::PSphere<float>(r){
    this->v = v;
    this->surf = s;

    this->m = m;
    x=0;
}


const GMlib::Vector<float, 3> &FirstBall::get_ds() const
{
    return ds;
}


void FirstBall::set_v(const GMlib::Vector<float, 3> &_setv)
{
    v=_setv;
}


float FirstBall::get_m()
{
    return m;
}


const GMlib::Vector<float, 3> &FirstBall::get_vel() const
{
    return v;
}

void FirstBall::update(float x){
    this->x=x;
}
void FirstBall::localSimulate(double dt){

    //    std::cout << "X: " << x << " ds: " << ds << std::endl;
    GMlib::Vector<float,3> step = (1-x)*ds;
    this->translateParent(step);

    //    std::cout << "Step length: " << step.getLength() << " radius: " << this->getRadius() << " axis: " << rolling_b << std::endl;
    this->rotateParent(GMlib::Angle(step.getLength()/this->getRadius()),rolling_b); //rotation of the ball
    x=0;

}

void FirstBall::bizzare(const GMlib::Vector<float,3>& p){


    surf->estimateClpPar(this->getPos(),_u,_v);
    surf->getClosestPoint(this->getPos(),_u,_v);
    GMlib::DMatrix<GMlib::Vector<float,3> > pointmat = surf->evaluate(_u,_v,1,1);
    n = pointmat[0][1]^ pointmat[1][0];

    GMlib::Vector<float,3> newpos = pointmat[0][0] + this->getRadius()*n;
    this->translate(newpos-getPos());

}



void FirstBall::update_step(double dt){
    const GMlib::Vector<float,3> g(0.0, 0.0, -9.81);

    ds = dt*v + (0.5*dt*dt)*g;

    p = this->getPos() + ds;
    surf->getClosestPoint(p,_u,_v);
    //        GMlib::Vector<float,3> d = p - surf->getCornerPoint();//get d=p-q
    //        _u = (d*surf->getU())/(surf->getU()*surf->getU());
    //        _v = (d*surf->getV())/(surf->getV()*surf->getV());

    GMlib::DMatrix<GMlib::Vector<float,3> > m = surf->evaluate(_u,_v,1,1);
    n = m[0][1]^m[1][0];

    GMlib::Vector<float,3> newpos = m[0][0] + this->getRadius()*n;

    //update v and ds and call translate

    ds = newpos - this->getPos();
    rolling_b =n^ds;

    v += dt*g;
    v -= (v*n)*n;
}



#endif
